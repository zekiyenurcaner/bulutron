<?php

namespace ApiBundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/api/files", name = "files")
     *@Method({"GET", "POST"})
     */
    public function indexAction(Request $request)
    {
      $response = new JsonResponse();

      $response->setStatusCode(Response::HTTP_OK);
      $response->headers->set('Content-Type', 'application/json');
      if(json_decode($request->getContent(), true)['email'] && json_decode($request->getContent(), true)['password']){
        $email = json_decode($request->getContent(), true)['email'];
        $password = json_decode($request->getContent(), true)['password'];
        $em = $this-> getDoctrine()->getManager();
        $person = $em->getRepository('AppBundle:Enduser')->findOneBy(array('email' => $email, 'password' => $password));
        if($person){
          $ayar = $person ->getPreferences()['api_enabled'];
          if($ayar === '1'){
          $filenames = [];
          $userid = $person ->getId();
          $files = $em->getRepository('AppBundle:cloudFile')->findBy(array('enduser' => $userid));


          foreach ($files as $file) {


             $filenames [] = [
               'filename' => $file->getFileName(),
               'filesize' => $file->getFileSize(),
               'uploaded_at'=> $file->getUploadTime(),
               's3_path' => 'bulutron/'.$file->getPath()
             ];

            }

          $response ->setData($filenames);
          return $response;
        }
        else{
          
          $response = new Response();
          $response->setStatusCode(401);
          return $response;
          
        }
      }
        else{
          
          $response = new Response();
          $response->setStatusCode(401);
          return $response;
          
        }
    }else{
      $response = new Response();
      $response->setStatusCode(404);
      return $response;
    }
  }

      /**
      *@Route("/api/quota" , name = "currentsize")
      *@Method({"GET","POST"})
      */
      public function sizeAction(Request $request)
      {
        $response = new JsonResponse();
        $response->setStatusCode(Response::HTTP_OK);
        $response->headers->set('Content-Type', 'application/json');

        if(json_decode($request->getContent(), true)['email'] && json_decode($request->getContent(), true)['password']){
          $data = json_decode($request->getContent(), true)['email'];
          $password = json_decode($request->getContent(), true)['password'];
          $em = $this-> getDoctrine()->getManager();
          $person = $em->getRepository('AppBundle:Enduser')->findOneBy(array('email' => $data, 'password' => $password));

          if($person){
            $ayar = $person ->getPreferences()['api_enabled'];
            if($ayar === '1'){
            $userid = $person ->getId();
            $toplamsize = 0;
            $files = $em->getRepository('AppBundle:cloudFile')->findBy(array('enduser' => $userid));
            $filecount = count($files);

            foreach ($files as $file) {
              $size = $file ->getFileSize();
              $toplamsize += $size;
            }

            $response ->setData(["file_count" => $filecount, "disk_usage" => $toplamsize]);
            return $response;
          }
        

          else{
          $response = new Response();
          $response->setStatusCode(401);
          return $response;
          }
        }
        }
        else{
        $response = new Response();
        $response->setStatusCode(401);
        return $response;
      }


      }

}
