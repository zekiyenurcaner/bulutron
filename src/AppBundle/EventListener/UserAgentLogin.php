<?php
namespace AppBundle\EventListener;


use AppBundle\Entity\Enduser;
use Doctrine\ORM\EntityManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Http\SecurityEvents;

class UserAgentLogin implements EventSubscriberInterface

{

private $em;

public function __construct(EntityManager $em)
{
$this->em = $em;
}
public function onInteractiveLogin(InteractiveLoginEvent $event)
{
/** @var Enduser $user */
$user = $event->getAuthenticationToken()->getUser();
$user->setLastLoggedIn(new \DateTime());
$this->em->persist($user);
$this->em->flush($user);

}
public static function getSubscribedEvents()
{
return array(SecurityEvents::INTERACTIVE_LOGIN => 'onInteractiveLogin');
}

}


?>
