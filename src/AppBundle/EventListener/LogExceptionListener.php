<?php
namespace AppBundle\EventListener;

use AppBundle\Entity\Enduser;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LogExceptionListener
{
  private $container;
  private $em;
  private $checker;
  private $storage;


  public function __construct(ContainerInterface $container,AuthorizationChecker $checker, EntityManager $em,TokenStorageInterface $storage){

      $this->container= $container;
      $this->checker = $checker;
      $this->em = $em;
      $this->storage = $storage;
  }

  public function onKernelResponse(FilterResponseEvent $event)
  { 
      
      $response  = $event->getResponse();
      $request   = $event->getRequest();
      $st = $response->getStatusCode();
      $pth = $request->getPathInfo();
      $date = $response->getDate()->format('d-m-Y@h:i:s');
      $ip = $request->getClientIp();

      if($ip == 'unknown'){
        $ip = $_SERVER['REMOTE_ADDR'];
      }

      $token=$this->storage->getToken();
     // var_dump($token);
      if($st !== 200){

      if (is_null($token) || $token->getUser() === 'anon.') {
        $mail = "Anonim";
      } else {
      
        $mail = $token->getUser()->getEmail();
    }
      $log = $st.'  '.$pth.'  '.$date.'  '.$ip.'  '.$mail;
      $path=$this->container->get('kernel')->getRootDir()."/../var/logs/bulutron-exceptions.log";
      file_put_contents($path, $log. PHP_EOL, FILE_APPEND | LOCK_EX);
      
  }

  }
}

 ?>
