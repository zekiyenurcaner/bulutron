<?php
namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\RadioType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\DependencyInjection\ContainerInterface;


class SettingsFormType extends AbstractType
{
    private $container;

    public function __construct(ContainerInterface $container){
        
              $this->container= $container;
          }
        
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $logger = $this->container->get('app.enduser_preferences');
        $sharing_enabled = $logger->getPreference()['sharing_enabled'];
        $sharing_notification_enabled = $logger->getPreference()['sharing_notification_enabled'];
        $api_enabled = $logger->getPreference()['api_enabled'];

        $builder
        ->add('sharing_enabled',ChoiceType::class,array('label'=> "Başkalarının benimle dosya paylaşmasına",
        'choices' => array(
          'İzin Veriyorum' => '1',
          'İzin Vermiyorum' => '0'
        ),
        'expanded' => true,
        'data'=> $sharing_enabled
 
    ))
    ->add('sharing_notification_enabled',ChoiceType::class,array('label'=> "Paylaştığım dosyalar erişildiğinde e-mail ile bilgilendirilmek",
       'choices' => array(
         'İstiyorum' => '1',
         'İstemiyorum' => '0'
       ),
       'expanded' => true,
       'data' => $sharing_notification_enabled
   ))
   ->add('api_enabled',ChoiceType::class,array('label'=> "Dosyalarıma ait bilgilere API ile erişime",
      'choices' => array(
        'İzin Veriyorum' => '1',
        'İzin Vermiyorum' => '0'
      ),
      'expanded' => true,
      'data' =>$api_enabled
  ))
  ->add('add', SubmitType::class,array('attr' => array('class' => 'btn btn-success btn-md'),'label' => 'Kaydet'))
        ;
    }
}







?>