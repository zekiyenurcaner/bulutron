<?php
namespace AppBundle\Form;

use AppBundle\Entity\Enduser;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RadioType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $row = 1;
        
            if (($handle = fopen("/home/zekiyenur/Desktop/data.csv", "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $num = count($data);
                $row++;
                for ($c=0; $c < $num; $c++) {
                    $citys[] = $data[$c];
                }
            }
            fclose($handle);
          }

        $builder
        ->add('name',     TextType::class,array('attr' => array('class' => 'form-control'),'required'=>false))
        ->add('email',    EmailType::class,array('attr' => array('class' => 'form-control'),'required'=>false))
        ->add('username', TextType::class,array('attr' => array('class' => 'form-control')))
        ->add('password', RepeatedType::class, array(
                 'type' => PasswordType::class,
                 'first_options'  => array('label' => 'Password','attr' => array('class' => 'form-control')),
                 'second_options' => array('label' => 'Repeat Password','attr' => array('class' => 'form-control')),
             ))
        ->add('sex',ChoiceType::class,array('attr' => array('style'=>'margin-top:10px'),
           'choices' => array(
             'Kadın' => '1',
             'Erkek' => '2'
           ),
           'multiple' => false,
           'expanded' => true
       ))
       ->add('madeinName',TextType::class,array('attr'=>array('class'=>'form-control','placeholder'=>'Önceki Soyadınız','style'=> 'display:none'),'required'=>false))
  
        ->add('city', ChoiceType::class,array('choices' =>array_combine($citys, $citys) ,'attr' => array('class' => 'form-control' ,'style' => 'margin-bottom:6px')))
        ->add('add', SubmitType::class,array('attr' => array('class' => 'btn btn-primary btn-md btn-block'),'label' => 'Kayıt'))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Enduser::class,
        ));
    }
}




?>