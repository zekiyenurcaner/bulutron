<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * cloudFile
 *
 * @ORM\Table(name="cloud_file")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\cloudFileRepository")
 */
class cloudFile
{
    /**
     * @ORM\ManyToOne(targetEntity="Enduser", inversedBy="cloudFiles" )
     * @ORM\JoinColumn(name="enduser_id", referencedColumnName="id")
     */
    private $enduser;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="upload_time", type="datetime")
     */
    private $uploadTime;

    /**
     * @var string
     *
     * @ORM\Column(name="file_size", type="string", length=255)
     */
    private $fileSize;

    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", length=255)
     */
    private $path;

    /**
     * @var string
     *
     * @ORM\Column(name="file_name", type="string", length=255)
     */
    private $fileName;

    /**
     * @var integer
     *
     * @ORM\Column(name="favorite", type="integer", length=11)
     */
    private $favorite;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set uploadTime
     *
     * @param \DateTime $uploadTime
     *
     * @return cloudFile
     */
    public function setUploadTime($uploadTime)
    {
        $this->uploadTime = $uploadTime;

        return $this;
    }

    /**
     * Get uploadTime
     *
     * @return \DateTime
     */
    public function getUploadTime()
    {
        return $this->uploadTime;
    }

    /**
     * Set fileSize
     *
     * @param string $fileSize
     *
     * @return cloudFile
     */
    public function setFileSize($fileSize)
    {
        $this->fileSize = $fileSize;

        return $this;
    }

    /**
     * Get fileSize
     *
     * @return string
     */
    public function getFileSize()
    {
        return $this->fileSize;
    }

    /**
     * Set path
     *
     * @param string $path
     *
     * @return cloudFile
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set fileName
     *
     * @param string $fileName
     *
     * @return cloudFile
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;

        return $this;
    }

    /**
     * Get fileName
     *
     * @return string
     */
    public function getFileName()
    {
        return $this->fileName;
    }


    /**
     * Set favorite
     *
     * @param integer $favorite
     *
     * @return cloudFile
     */
    public function setFavorite($favorite)
    {
        $this->favorite = $favorite;

        return $this;
    }

    /**
     * Get favorite
     *
     * @return integer
     */
    public function getFavorite()
    {
        return $this->favorite;
    }


    /**
     * Get enduser
     *
     * @return \AppBundle\Entity\Enduser
     */
    public function getEnduser()
    {
        return $this->enduser;
    }


    /**
     * Set enduser
     *
     * @param \AppBundle\Entity\Enduser $enduser
     *
     * @return cloudFile
     */
    public function setEnduser(\AppBundle\Entity\Enduser $enduser = null)
    {
        $this->enduser = $enduser;

        return $this;
    }


}
