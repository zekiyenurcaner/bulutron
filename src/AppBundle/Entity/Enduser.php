<?php

namespace AppBundle\Entity;


use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;


/**
 * Enduser
 *
 * @ORM\Table(name="enduser")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\EnduserRepository")
 */
class Enduser implements AdvancedUserInterface, \Serializable
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="cloudFile", mappedBy="enduser")
     */
    private $cloudFiles;

    public function __construct()
    {
        $this->cloudFiles = new ArrayCollection();
    }

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=255)
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255)
     */
    private $password;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="lastLoggedIn", type="datetime")
     */
    private $lastLoggedIn;

    /**
    *@var string
    *
    * @ORM\Column(name="email", type="string", length=255, unique=true)
    */
    private $email;

    /**
    *@var string
    *
    * @ORM\Column(name="city", type="string", length=255)
    */
    private $city;

    /**
    *@var int
    *
    * @ORM\Column(name="sex", type="integer")
    */
    private $sex;


    /**
    *@var string
    *
    * @ORM\Column(name="madeinName", type="string", length=255)
    */
    private $madeinName;


    /**
    *@var array
    *
    * @ORM\Column(name="preferences", type="json_array")
    */
    private $preferences;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Enduser
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set username
     *
     * @param string $username
     *
     * @return Enduser
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return Enduser
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }
    /**
     * Set lastLoggedIn
     *
     * @param \DateTime $lastLoggedIn
     *
     * @return Enduser
     */
    public function setLastLoggedIn($lastLoggedIn)
    {
         $this->lastLoggedIn = $lastLoggedIn;

          return $this;
    }

    /**
     * Get lastLoggedIn
     *
     * @return \DateTime
     */
    public function getLastLoggedIn()
    {
        return $this->lastLoggedIn;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Enduser
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }


    /**
     * Set city
     *
     * @param string $city
     *
     * @return Enduser
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }


    /**
     * Set sex
     *
     * @param integer $sex
     *
     * @return Sex
     */
    public function setSex($sex)
    {
        $this->sex = $sex;

        return $this;
    }

    /**
     * Get sex
     *
     * @return int
     */
    public function getSex()
    {
        return $this->sex;
    }


    /**
     * Set madeinName
     *
     * @param string $madeinName
     *
     * @return Enduser
     */
    public function setMadeinName($madeinName)
    {
        $this->madeinName = $madeinName;

        return $this;
    }

    /**
     * Get madeinName
     *
     * @return string
     */
    public function getMadeinName()
    {
        return $this->madeinName;
    }

    /**
     * Set preferences
     *
     * @param array $preferences
     *
     * @return Enduser
     */
    public function setPreferences($preferences)
    {
        $this->preferences = $preferences;

        return $this;
    }

    /**
     * Get preferences
     *
     * @return array
     */
    public function getPreferences()
    {
        return $this->preferences;
    }



    /**
     * Checks whether the user's account has expired.
     *
     * Internally, if this method returns false, the authentication system
     * will throw an AccountExpiredException and prevent login.
     *
     * @return bool true if the user's account is non expired, false otherwise
     *
     * @see AccountExpiredException
     */
    public function isAccountNonExpired()
    {
        // TODO: Implement isAccountNonExpired() method.
        return true;
    }

    /**
     * Checks whether the user is locked.
     *
     * Internally, if this method returns false, the authentication system
     * will throw a LockedException and prevent login.
     *
     * @return bool true if the user is not locked, false otherwise
     *
     * @see LockedException
     */
    public function isAccountNonLocked()
    {
        // TODO: Implement isAccountNonLocked() method.
        return true;
    }

    /**
     * Checks whether the user's credentials (password) has expired.
     *
     * Internally, if this method returns false, the authentication system
     * will throw a CredentialsExpiredException and prevent login.
     *
     * @return bool true if the user's credentials are non expired, false otherwise
     *
     * @see CredentialsExpiredException
     */
    public function isCredentialsNonExpired()
    {
        // TODO: Implement isCredentialsNonExpired() method.
        return true;
    }

    /**
     * Checks whether the user is enabled.
     *
     * Internally, if this method returns false, the authentication system
     * will throw a DisabledException and prevent login.
     *
     * @return bool true if the user is enabled, false otherwise
     *
     * @see DisabledException
     */
    public function isEnabled()
    {
        // TODO: Implement isEnabled() method.
        return true;
    }

    /**
     * String representation of object
     * @link http://php.net/manual/en/serializable.serialize.php
     * @return string the string representation of the object or null
     * @since 5.1.0
     */
    public function serialize()
    {
        // TODO: Implement serialize() method.
        return serialize(array(
            $this->id,
            $this->username,
            $this->password,
            // see section on salt below
            // $this->salt,
        ));
    }

    /**
     * Constructs the object
     * @link http://php.net/manual/en/serializable.unserialize.php
     * @param string $serialized <p>
     * The string representation of the object.
     * </p>
     * @return void
     * @since 5.1.0
     */
    public function unserialize($serialized)
    {
        // TODO: Implement unserialize() method.
        list (
            $this->id,
            $this->username,
            $this->password,
            // see section on salt below
            // $this->salt
            ) = unserialize($serialized);
    }

    /**
     * Returns the roles granted to the user.
     *
     * <code>
     * public function getRoles()
     * {
     *     return array('ROLE_USER');
     * }
     * </code>
     *
     * Alternatively, the roles might be stored on a ``roles`` property,
     * and populated in any number of different ways when the user object
     * is created.
     *
     * @return (Role|string)[] The user roles
     */
    public function getRoles()
    {
        // TODO: Implement getRoles() method.
        return array("ROLE_ADMIN");
    }

    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string|null The salt
     */
    public function getSalt()
    {
      return null;
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {
        $this->Password = null;
    }

    /**
     * Add cloudFile
     *
     * @param \AppBundle\Entity\cloudFile $cloudFile
     *
     * @return Enduser
     */
    public function addCloudFile(\AppBundle\Entity\cloudFile $cloudFile)
    {
        $this->cloudFiles[] = $cloudFile;

        return $this;
    }

    /**
     * Remove cloudFile
     *
     * @param \AppBundle\Entity\cloudFile $cloudFile
     */
    public function removeCloudFile(\AppBundle\Entity\cloudFile $cloudFile)
    {
        $this->cloudFiles->removeElement($cloudFile);
    }

    /**
     * Get cloudFiles
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCloudFiles()
    {
        return $this->cloudFiles;
    }
}
