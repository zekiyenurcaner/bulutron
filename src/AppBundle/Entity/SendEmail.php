<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SendEmail
 *
 * @ORM\Table(name="send_email")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SendEmailRepository")
 */
class SendEmail
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="string", length=255)
     */
    private $content;

    /**
     * @var string
     *
     * @ORM\Column(name="file", type="string", length=255)
     */
    private $file;


    /**
     * @var string
     *
     * @ORM\Column(name="gonderen", type="string", length=255)
     */
    private $gonderen;

    /**
     * @var string
     *
     * @ORM\Column(name="alici", type="string", length=255)
     */
    private $alici;

    /**
     * @var int
     *
     * @ORM\Column(name="durum", type="integer")
     */
    private $durum;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return SendEmail
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }


    /**
     * Set file
     *
     * @param string $file
     *
     * @return SendEmail
     */
    public function setFile($file)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Get file
     *
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }


    /**
     * Set gonderen
     *
     * @param string $gonderen
     *
     * @return SendEmail
     */
    public function setGonderen($gonderen)
    {
        $this->gonderen = $gonderen;

        return $this;
    }

    /**
     * Get gonderen
     *
     * @return string
     */
    public function getGonderen()
    {
        return $this->gonderen;
    }

    /**
     * Set alici
     *
     * @param string $alici
     *
     * @return SendEmail
     */
    public function setAlici($alici)
    {
        $this->alici = $alici;

        return $this;
    }

    /**
     * Get alici
     *
     * @return string
     */
    public function getAlici()
    {
        return $this->alici;
    }

    /**
     * Set durum
     *
     * @param integer $durum
     *
     * @return SendEmail
     */
    public function setDurum($durum)
    {
        $this->durum = $durum;

        return $this;
    }

    /**
     * Get durum
     *
     * @return int
     */
    public function getDurum()
    {
        return $this->durum;
    }
}
