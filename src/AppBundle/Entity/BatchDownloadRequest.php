<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BatchDownloadRequest
 *
 * @ORM\Table(name="batch_download_request")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BatchDownloadRequestRepository")
 */
class BatchDownloadRequest
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="dosya_id", type="integer")
     */
    private $dosyaId;

    /**
     * @var int
     *
     * @ORM\Column(name="person_id", type="integer")
     */
    private $personId;


    /**
     * @var string
     *
     * @ORM\Column(name="dosya_adi", type="string", length=255)
     */
    private $dosyaAdi;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="integer")
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="dosya_path", type="string", length=255)
     */
    private $dosyaPath;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dosyaId
     *
     * @param integer $dosyaId
     *
     * @return BatchDownloadRequest
     */
    public function setDosyaId($dosyaId)
    {
        $this->dosyaId = $dosyaId;

        return $this;
    }

    /**
     * Get dosyaId
     *
     * @return int
     */
    public function getDosyaId()
    {
        return $this->dosyaId;
    }


    /**
     * Set personId
     *
     * @param integer $personId
     *
     * @return BatchDownloadRequest
     */
    public function setPersonId($personId)
    {
        $this->personId = $personId;

        return $this;
    }

    /**
     * Get personId
     *
     * @return int
     */
    public function getPersonId()
    {
        return $this->personId;
    }

    /**
     * Set dosyaAdi
     *
     * @param string $dosyaAdi
     *
     * @return BatchDownloadRequest
     */
    public function setDosyaAdi($dosyaAdi)
    {
        $this->dosyaAdi = $dosyaAdi;

        return $this;
    }

    /**
     * Get dosyaAdi
     *
     * @return string
     */
    public function getDosyaAdi()
    {
        return $this->dosyaAdi;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return BatchDownloadRequest
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set dosyaPath
     *
     * @param string $dosyaPath
     *
     * @return BatchDownloadRequest
     */
    public function setDosyaPath($dosyaPath)
    {
        $this->dosyaPath = $dosyaPath;

        return $this;
    }

    /**
     * Get dosyaPath
     *
     * @return string
     */
    public function getDosyaPath()
    {
        return $this->dosyaPath;
    }
}
