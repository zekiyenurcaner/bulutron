<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SharedCloudFile
 *
 * @ORM\Table(name="shared_cloud_file")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SharedCloudFileRepository")
 */
class SharedCloudFile
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="sharedfile", type="string", length=255)
     */
    private $sharedfile;

    /**
     * @var string
     *
     * @ORM\Column(name="paylasankul", type="string", length=255)
     */
    private $paylasankul;

    /**
     * @var string
     *
     * @ORM\Column(name="paylasilankul", type="string", length=255)
     */
    private $paylasilankul;

    /**
     * @var int
     *
     * @ORM\Column(name="kackere", type="integer")
     */
    private $kackere;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sharedfile
     *
     * @param string $sharedfile
     *
     * @return SharedCloudFile
     */
    public function setSharedfile($sharedfile)
    {
        $this->sharedfile = $sharedfile;

        return $this;
    }

    /**
     * Get sharedfile
     *
     * @return string
     */
    public function getSharedfile()
    {
        return $this->sharedfile;
    }



    /**
     * Set paylasankul
     *
     * @param string $paylasankul
     *
     * @return SharedCloudFile
     */
    public function setPaylasankul($paylasankul)
    {
        $this->paylasankul = $paylasankul;

        return $this;
    }

    /**
     * Get paylasankul
     *
     * @return string
     */
    public function getPaylasankul()
    {
        return $this->paylasankul;
    }

    /**
     * Set paylasılankul
     *
     * @param string $paylasılankul
     *
     * @return SharedCloudFile
     */
    public function setPaylasilankul($paylasilankul)
    {
        $this->paylasilankul = $paylasilankul;

        return $this;
    }

    /**
     * Get paylasılankul
     *
     * @return string
     */
    public function getPaylasilankul()
    {
        return $this->paylasilankul;
    }

    /**
     * Set kackere
     *
     * @param integer $kackere
     *
     * @return SharedCloudFile
     */
    public function setKackere($kackere)
    {
        $this->kackere = $kackere;

        return $this;
    }

    /**
     * Get kackere
     *
     * @return int
     */
    public function getKackere()
    {
        return $this->kackere;
    }
}
