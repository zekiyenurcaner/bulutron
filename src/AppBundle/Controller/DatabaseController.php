<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Enduser;
use AppBundle\Entity\cloudFile;
use AppBundle\Entity\SharedCloudFile;
use AppBundle\Entity\BatchDownloadRequest;
use AppBundle\Entity\SendEmail;
use AppBundle\Form\UserType;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


class DatabaseController extends Controller
{
    /**
     * @Route("/index", name = "index" )
     *@Method({"GET", "POST"})
     */
    public function uploadAction(Request $request){
    
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if($user ==="anon."){
          $response = new Response('',500);
          return $this->render("AppBundle:Exception:error500.html.twig",[],$response);
        }
        else{
          $userId = $user->getId();
        $register =new cloudFile();
        $form = $this->createFormBuilder($register)
          ->add('fileName', FileType::class)
          ->add('add', SubmitType::class,array('attr' => array('class' => 'btn btn-primary btn-xs')))
          ->getForm();
        $form->handleRequest($request);

    if($request->isMethod('POST') && $form->isSubmitted() && $form->isValid())
    {

        $client = new S3Client(array(
                  'version' =>'latest',
                  'signature'=> "v2",
                  'region'  =>'us-east-2',
                  'credentials' => array(
                  'key'=>$this->container->getParameter('amazon.s3.key'),
                  'secret'=>$this->container->getParameter('amazon.s3.secret')
                ),
                 'aws:SecureTransport'=>true,

        ));

       $files=$request->files->all();
       $file=$files['form']['fileName'];
       $fileName = md5(uniqid()).'.'.$file->guessExtension();
       $name=$file->getClientOriginalName();
       $fileSize= $file->getClientSize();
       $file->move("/home/zekiyenur/Desktop/files",$fileName);
       $result =$client ->putObject(array(
             'Bucket' => 'bulutron',
             'Key'    => $fileName,
             'Body'   => fopen('/home/zekiyenur/Desktop/files/'.$fileName,'rb'),
             'ACL'   => ''
         ));
         //unlink('/home/zekiyenur/bulutron/files/'.$fileName);

     $now = new\DateTime('now');
     $register->setFileName($name);
     $register->setUploadTime($now);
     $register->setFileSize($fileSize);
     $register->setPath($fileName);
     $register->setEnduser($user);
     $register ->setFavorite(0);
     $em = $this-> getDoctrine()->getManager();
     $em -> persist($register);
     $em ->flush();
     $this -> AddFlash(
        'notice',
        'Added'
      );

    }
    $emm = $this-> getDoctrine()->getManager();
    $list= $emm->getRepository('AppBundle:cloudFile')->findBy(array('enduser'=>$userId), array('uploadTime' => 'DESC'));

    return $this->render('AppBundle:bulutron:index.html.twig', array(
        'list'=>$list,
        'form' => $form->createView(),
        'userId'=> $userId

      ));
   }
  }


        /**
         * @Route("/delete/{id}", name= "delete")
         */
         public function deleteAction($id)
        {
             $em = $this-> getDoctrine()->getManager();
             $list = $em -> getRepository('AppBundle:cloudFile')->find($id);
             $em -> remove($list);
             $em -> flush();
             $this -> AddFlash(
                'notice',
                'Deleted'
              );
             return $this -> redirect('/index');
        }

    /**
     * @Route("/download/{link}", name="enduser")
     * @Method({"GET", "POST"})
     */
    public function showAction($link)
    {
      $user = $this->get('security.token_storage')->getToken()->getUser();
      $userId = $user->getId();
      $em = $this-> getDoctrine()->getManager();
      $file = $em->getRepository('AppBundle:cloudFile')->findOneBy(array('path' => $link));
      $enduserId = $file->getEnduser()->getId();

      if($enduserId === $userId){
          $client = new S3Client(array(
                       'version' =>'latest',
                       'signature'=> "v2",
                       'region'  =>'us-east-2',
                       'credentials' => array(
                       'key'=>$this->container->getParameter('amazon.s3.key'),
                       'secret'=>$this->container->getParameter('amazon.s3.secret'),
                  ),
                      'aws:SecureTransport'=>false,
                  ));

          $cmd = $client->getCommand('GetObject', [
          'Bucket' => 'bulutron',
          'Key'    => $link,
          'credentials' => array(
          'key' => $this ->container ->getParameter('amazon.s3.key'),
          'secret'=>$this ->container->getParameter('amazon.s3.secret'),
          ),
          'ACL'=> "public-read-write",
          'aws:SecureTransport'=>false,
          ]);


              $request = $client->createPresignedRequest($cmd, '+1 minute');

          $presignedUrl = (string) $request->getUri();
          return $this->redirect($presignedUrl);

    }
    else {
      $response = new Response('',404);
      return $this->render("AppBundle:Exception:error404.html.twig",[],$response);
    }

    }

    /**
    *@Route("/favorite" , name= "fav")
    *@Method({"GET", "POST"})
    */

    public function favAction(Request $request)
    {
      $dosya_id=$request->get('dosya_id');
      $em = $this-> getDoctrine()->getManager();
      $bul = $em->getRepository('AppBundle:cloudFile')->findOneBy(array('id' => $dosya_id));
      $dosya=$bul->getFavorite();

      if($dosya == 0){
        $bul->setFavorite(1);
        $emm = $this-> getDoctrine()->getManager();
        $emm -> persist($bul);
        $emm ->flush();
        $response = new JsonResponse();
        return $response;
      }
      if($dosya == 1){
        $bul->setFavorite(0);
        $emm = $this-> getDoctrine()->getManager();
        $emm -> persist($bul);
        $emm ->flush();
        $response = new JsonResponse();
        return $response;
      }
    }

    /**
    *@Route("/paylas" , name= "paylas")
    *@Method({"GET", "POST"})
    */

    public function paylasAction(Request $request )
    {


      $user = $this->get('security.token_storage')->getToken()->getUser()->getId();
      $em = $this-> getDoctrine()->getManager();
      $kisi=  $em->getRepository('AppBundle:Enduser')->findOneBy(array('id' => $user));
      $atayan = $kisi ->getEmail($kisi);
      $g_username = $kisi ->getUsername($kisi);
      $g_name = $kisi ->getName($kisi);

      $dosyaname = $request->get('dosya_name');
      $dosyapath = $em ->getRepository('AppBundle:cloudFile')->findOneBy(array('fileName' => $dosyaname ));
      $uzanti = $dosyapath ->getPath($dosyapath);

      $em = $this-> getDoctrine()->getManager();
      $emailval=$request->get('emailval');
      $email = $em->getRepository('AppBundle:Enduser')->findOneBy(array('email' => $emailval));
      $ayar = $email->getPreferences()['sharing_enabled'];

      if($ayar === "1"){
      $adress = $email->getEmail();
      $aliciId = $email ->getId();

      if($adress != null){

        $client = new S3Client(array(
                     'version' =>'latest',
                     'signature'=> "v2",
                     'region'  =>'us-east-2',
                     'credentials' => array(
                     'key'=>$this->container->getParameter('amazon.s3.key'),
                     'secret'=>$this->container->getParameter('amazon.s3.secret'),
                ),
                    'aws:SecureTransport'=>false,
                ));

        $cmd = $client->getCommand('GetObject', [
        'Bucket' => 'bulutron',
        'Key'    => $uzanti,
        'credentials' => array(
        'key' => $this ->container ->getParameter('amazon.s3.key'),
        'secret'=>$this ->container->getParameter('amazon.s3.secret'),
        ),
        'ACL'=> "public-read-write",
        'aws:SecureTransport'=>false,
        ]);
          $request = $client->createPresignedRequest($cmd, '+1 hour');

          $presignedUrl = (string) $request->getUri();

          $url = "http://127.0.0.1:8000/p/download/".$uzanti.'/'.$aliciId;

        $kayit = new SendEmail();
        $kayit->setContent($g_username.' '.$g_name.' '.'kişisi tarafından şu link gönderildi'.' '.$url);
        $kayit->setGonderen($atayan);
        $kayit->setAlici($adress);
        $kayit->setFile('boş');
        $kayit->setDurum(0);
        $emm = $this-> getDoctrine()->getManager();
        $emm -> persist($kayit);
        $emm ->flush();


      $reg =new SharedCloudFile();
      $reg->setSharedfile($uzanti);
      $reg->setPaylasankul($atayan);
      $reg->setPaylasilankul($adress);
      $reg->setKackere(0);
      $emm = $this-> getDoctrine()->getManager();
      $emm -> persist($reg);
      $emm ->flush();

      $response = new JsonResponse();
      return $response;
    }

  }
    }

    /**
    *@Route("/gonder" , name= "gonder")
    *@Method({"GET", "POST"})
    */

    public function gonderAction(Request $request )
    {
      $user = $this->get('security.token_storage')->getToken()->getUser()->getId();
      $em = $this-> getDoctrine()->getManager();
      $kisi=  $em->getRepository('AppBundle:Enduser')->findOneBy(array('id' => $user));
      $atayan = $kisi ->getEmail($kisi);
      $g_username = $kisi ->getUsername($kisi);
      $g_name = $kisi ->getName($kisi);

      $dpath = $request->get('dosya_path');
      $fpath = $em ->getRepository('AppBundle:cloudFile')->findOneBy(array('path' => $dpath ));
      $spath = $fpath ->getPath($fpath);

      $em = $this-> getDoctrine()->getManager();
      $email=$request->get('email');
      $eadres = $em->getRepository('AppBundle:Enduser')->findOneBy(array('email' => $email));
      $gonderilen = $eadres->getEmail($eadres);


      if($gonderilen != null){

          $url = '/home/zekiyenur/Desktop/files/'.$spath;

        $kayit = new SendEmail();
        $kayit->setContent($g_username.' '.$g_name.' '.'kişisi tarafından ekteki dosya gönderildi');
        $kayit->setGonderen($atayan);
        $kayit->setAlici($gonderilen);
        $kayit->setFile($url);
        $kayit->setDurum(0);
        $emm = $this-> getDoctrine()->getManager();
        $emm -> persist($kayit);
        $emm ->flush();


      $reg =new SharedCloudFile();
      $reg->setSharedfile($spath);
      $reg->setPaylasankul($atayan);
      $reg->setPaylasilankul($gonderilen);
      $reg->setKackere(0);
      $emm = $this-> getDoctrine()->getManager();
      $emm -> persist($reg);
      $emm ->flush();

      $response = new JsonResponse();
      return $response;
    }
  }
  /**
  *@Route("/toplu" , name= "toplu")
  *@Method({"GET", "POST"})
  */

  public function topluGonderAction(Request $request )
  {
        $user = $this->get('security.token_storage')->getToken()->getUser()->getId();
        $val = $request->get('val');
        $em = $this-> getDoctrine()->getManager();
        $file =  $em->getRepository('AppBundle:cloudFile')->findOneBy(array('id' => $val));
        $filename = $file ->getFileName($file);
        $filepath = $file ->getPath($file);

        $bachkayit = new BatchDownloadRequest();
        $bachkayit ->setDosyaId($val);
        $bachkayit ->setPersonId($user);
        $bachkayit ->setDosyaAdi($filename);
        $bachkayit ->setStatus(0);
        $bachkayit ->setDosyaPath($filepath);
        $em ->persist($bachkayit);
        $em ->flush();


        $response = new JsonResponse();
        $response->setData(['dosya value' => $val]);
        return $response;
  }


  /**
  *@Route("/newuser", name = "newuser")
  *@Method({"GET","POST"})
  */

  public function newUserAction(Request $request)
  { 
    $passwordEncoder = $this->get('security.password_encoder');
    $user = new Enduser();
    $form = $this->createForm(UserType::class, $user);
    $form->handleRequest($request);

    if($form->isSubmitted() && $form->isValid())
    {
      $madeinName = $form['madeinName']->getData();
      $pwd = $form['password']->getData();
      $name =$form['name']->getData();
      $username =$form['username']->getData();
      $email =$form['email']->getData();
      $city = $form['city']->getData();
      $sex = $form['sex']->getData();

      $em = $this-> getDoctrine()->getManager();

      if($name == ""){
        $this -> AddFlash(
          'success',
           'İsim Boş Geçilemez'
       );
      }
      if($email == ""){
        $this -> AddFlash(
          'success',
           'Email Boş Geçilemez'
       );
      }
      $mail =  $em->getRepository('AppBundle:Enduser')->findOneBy(array('email' => $email));
      if($mail){
        $this -> AddFlash(
          'success',
           'Önceden Kayıtlı mail adresi'
       );
      }

      $uname = $em->getRepository('AppBundle:Enduser')->findOneBy(array('username' => $username));
      if($uname){
        $this -> AddFlash(
          'alert',
           'Kullanılan Username'
       );
      }

      if(!$mail && $name != "" && $email != ""){
        if(!$uname){
          if(strlen($pwd)>5 && strlen($pwd)<10){
            if($sex == '1' && $madeinName != ""){
              $pw = $passwordEncoder->encodePassword($user, $user->getPassword());
              $arr = ['sharing_enabled'=> "1", 'sharing_notification_enabled'=> "1", 'api_enabled'=> "0"];
              $now = new\DateTime('now');
              $userkayit = new Enduser();
              $userkayit ->setName($name);
              $userkayit ->setUsername($username);
              $userkayit ->setPassword($pw);
              $userkayit ->setLastLoggedIn($now);
              $userkayit ->setEmail($email);
              $userkayit ->setCity($city);
              $userkayit ->setMadeinName($madeinName);
              $userkayit ->setSex($sex);
              $userkayit ->setPreferences($arr);
              $em ->persist($userkayit);
              $em ->flush();
              $token = new UsernamePasswordToken(
                $userkayit,
                $pwd,
                'main',
                $userkayit->getRoles()
              );

              $this->get('security.token_storage')->setToken($token);
              $this->get('session')->set('_security_main', serialize($token));
              $this->addFlash('success', 'Giriş Yaptınız!');
              return $this->redirect('/index');
            }
            elseif($sex == '2' && $madeinName == ""){
              $pw = $passwordEncoder->encodePassword($user, $user->getPassword());              
              $arr = ['sharing_enabled'=> "1", 'sharing_notification_enabled'=> "1", 'api_enabled'=> "0"];
              $now = new\DateTime('now');
              $userkayit = new Enduser();
              $userkayit ->setName($name);
              $userkayit ->setUsername($username);
              $userkayit ->setPassword($pw);
              $userkayit ->setLastLoggedIn($now);
              $userkayit ->setEmail($email);
              $userkayit ->setCity($city);
              $userkayit ->setMadeinName("yok");
              $userkayit ->setSex($sex);
              $userkayit ->setPreferences($arr);
              $em ->persist($userkayit);
              $em ->flush();
              $token = new UsernamePasswordToken(
                $userkayit,
                $pwd,
                'main',
                $userkayit->getRoles()
              );

              $this->get('security.token_storage')->setToken($token);
              $this->get('session')->set('_security_main', serialize($token));
              $this->addFlash('success', 'Giriş Yaptınız!');
              return $this->redirect('/index');
            }
          else {
            $this -> AddFlash(
              'alert',
               'Hatalı İşlem'
           );
          }
          }
          else{
            $this -> AddFlash(
              'alert',
               'Şifreyi 5-10 hane arasında giriniz.'
           );
          }
        }
      }


    }
  return $this ->render('AppBundle:bulutron:newuser.html.twig',array(
    'form' => $form->createView()
    ));

}
   /**
     * @Route("/anasayfa", name="anasayfa")
     * @Method({"GET", "POST"})
     */
    public function anasayfa()
    {
      return $this ->render('AppBundle:bulutron:anasayfa.html.twig');
    }











}

?>
