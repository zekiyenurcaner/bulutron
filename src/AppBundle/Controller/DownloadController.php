<?php
namespace AppBundle\Controller;

use AppBundle\Entity\Enduser;
use AppBundle\Entity\cloudFile;
use AppBundle\Entity\SharedCloudFile;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\JsonResponse;


class DownloadController extends Controller
{

  /**
  *@Route("/p/download/{filepath}/{aliciId}" , name= "dwn")
  */
  public function downloadAction($filepath,$aliciId){
    $client = new S3Client(array(
                 'version' =>'latest',
                 'signature'=> "v2",
                 'region'  =>'us-east-2',
                 'credentials' => array(
                 'key'=>$this->container->getParameter('amazon.s3.key'),
                 'secret'=>$this->container->getParameter('amazon.s3.secret'),
            ),
                'aws:SecureTransport'=>false,
            ));

    $cmd = $client->getCommand('GetObject', [
    'Bucket' => 'bulutron',
    'Key'    => $filepath,
    'credentials' => array(
    'key' => $this ->container ->getParameter('amazon.s3.key'),
    'secret'=>$this ->container->getParameter('amazon.s3.secret'),
    ),
    'ACL'=> "public-read-write",
    'aws:SecureTransport'=>false,
    ]);


    $request = $client->createPresignedRequest($cmd, '+1 hour');

    $presignedUrl = (string) $request->getUri();

    $em = $this-> getDoctrine()->getManager();
    $eml = $em ->getRepository('AppBundle:Enduser')->findOneBy(array('id'=> $aliciId));
    $ml = $eml ->getEmail($eml);
    $dwn =  $em->getRepository('AppBundle:SharedCloudFile')->findOneBy(array('sharedfile' => $filepath,'paylasilankul'=>$ml));
    $dwncount = $dwn ->getKackere($dwn);

    if ($dwncount === 0){
      $gonderen = $dwn->getPaylasankul($dwn);
      $alici = $dwn->getPaylasilankul($dwn);
      $ayarlar = $em->getRepository('AppBundle:Enduser')->findOneBy(array('email' => $gonderen));
      $ayar = $ayarlar->getPreferences()['sharing_notification_enabled'];

      if($ayar === "1"){

      $mail = new PHPMailer(true);
      $mail->IsSMTP();
          $mail->Host       = "email-smtp.us-east-1.amazonaws.com";
          $mail->CharSet    = 'UTF-8';
          $mail->Port       =  587;
          $mail->Username   = "AKIAIBOVVFSZ5TO7QNGA";
          $mail->Password   = "Aj5VglgRS+y3Kg4Q1vJFJZRaZ7h2KGHb5994iQGd17Tm";
          $mail->SMTPDebug  = 0;
          $mail->SMTPAuth   = true;
          $mail->SMTPSecure = "tls";

        $mail->setFrom($alici);
        $mail->addAddress($gonderen);
        $mail->addReplyTo($gonderen);


          $mail->Subject = "Dosya Açıldı.";
          $mail->Body    =  "Gönderilen dosya açılmıştır.";
          $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

          $mail->Send();
        }
          $dwncount = $dwncount + 1;
          $bul = $em->getRepository('AppBundle:SharedCloudFile')->findOneBy(array('sharedfile' => $filepath,'paylasilankul'=>$ml));
          $bul->setKackere($dwncount);
          $emm = $this-> getDoctrine()->getManager();
          $emm -> persist($bul);
          $emm ->flush();
          return $this->redirect($presignedUrl);

  }
  else if($dwncount > 0){
    $gonderen = $dwn->getPaylasankul($dwn);
    $alici = $dwn->getPaylasilankul($dwn);
    $dwncount = $dwncount + 1;
    $bul = $em->getRepository('AppBundle:SharedCloudFile')->findOneBy(array('sharedfile' => $filepath,'paylasilankul'=>$ml));
    $bul->setKackere($dwncount);
    $emm = $this-> getDoctrine()->getManager();
    $emm -> persist($bul);
    $emm ->flush();
    return $this->redirect($presignedUrl);

  }

  }





}



























 ?>
