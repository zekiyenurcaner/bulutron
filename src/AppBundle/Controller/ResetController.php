<?php
namespace AppBundle\Controller;

use AppBundle\Entity\Enduser;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\JsonResponse;

class ResetController extends Controller
{
  /**
   * @Route("/reset", name = "reset" )
   *@Method({"GET", "POST"})
   */
  public function ResettingAction(Request $request){
    $reset = $this->createFormBuilder()
   ->add('email',    EmailType::class,array('attr' => array('class' => 'form-control')))
   ->add('username', TextType::class,array('attr' => array('class' => 'form-control')))
    ->add('add', SubmitType::class,array('attr' => array('class' => 'btn btn-primary btn-md btn-block','style' => 'margin-top:6px'),'label' => 'Kullanıcı Doğrulama'))
    ->getForm();

     $reset->handleRequest($request);

     if($reset->isSubmitted() && $reset->isValid())
     {
       $username =$reset['username']->getData();
       $email =$reset['email']->getData();

       $em = $this-> getDoctrine()->getManager();
       $mail =  $em->getRepository('AppBundle:Enduser')->findOneBy(array('email' => $email,'username'=>$username));
       if($mail){
         $id = $mail -> getId($mail);
         return $this->redirect('/degistir'.'/'.$id);
       }
       elseif(!$mail){
         $this -> AddFlash(
           'notice',
            'Geçersiz Kullanıcı Bilgileri'
        );
       }

     }



     return $this ->render('AppBundle:bulutron:reset.html.twig',array(
       'form' => $reset->createView()
       ));

}

/**
 * @Route("/degistir/{id}", name = "degistir" )
 *@Method({"GET", "POST"})
 */
  public function DegistirAction(Request $request, $id){

    $degistir = $this->createFormBuilder()
    ->add('password', RepeatedType::class, array(
             'type' => PasswordType::class,
             'first_options'  => array('label' => 'Password','attr' => array('class' => 'form-control')),
             'second_options' => array('label' => 'Repeat Password','attr' => array('class' => 'form-control')),
         ))
    ->add('add', SubmitType::class,array('attr' => array('class' => 'btn btn-primary btn-md btn-block','style' => 'margin-top:6px'),'label' => 'Değiştir'))
    ->getForm();

       $degistir->handleRequest($request);

       if($degistir->isSubmitted() && $degistir->isValid())
       {

       $pwd =$degistir['password']->getData();

       $em = $this-> getDoctrine()->getManager();
       $alici =  $em->getRepository('AppBundle:Enduser')->findOneBy(array('id' => $id));
       $receiver = $alici ->getEmail($alici);

       $mail = new PHPMailer(true);
       $mail->IsSMTP();
       $mail->Host       = "email-smtp.us-east-1.amazonaws.com";
       $mail->CharSet    = 'UTF-8';
       $mail->Port       =  587;
       $mail->Username   = "AKIAIBOVVFSZ5TO7QNGA";
       $mail->Password   = "Aj5VglgRS+y3Kg4Q1vJFJZRaZ7h2KGHb5994iQGd17Tm";
       $mail->SMTPDebug  = 0;
       $mail->SMTPAuth   = true;
       $mail->SMTPSecure = "tls";

       $mail->setFrom($receiver);
       $mail->addAddress($receiver);
       $mail->addReplyTo($receiver);


       $mail->Subject = "Bulutron Şifre Değişikliği";
       $mail->Body    =  "Yeni Şifreniz:".''.$pwd;
       $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

      if($mail->Send()){
        $this -> AddFlash(
          'success',
           'Şifreniz mail adresinize gönderilmiştir.'
       );
      }
     }

   return $this ->render('AppBundle:bulutron:degistir.html.twig',array(
     'form' => $degistir->createView()
     ));

}



}

 ?>
