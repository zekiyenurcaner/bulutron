<?php
namespace AppBundle\Controller;

use AppBundle\AppBundle;
use AppBundle\Entity\Enduser;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;



class SecurityController extends Controller
{
    /**
     * @Route("/login" , name= "login")
     */
    public function loginAction(Request $request)
    {
        $form = $this->createFormBuilder()
        ->add('_username',     TextType::class,array('attr' => array('class' => 'form-control')))
        
        ->add('_password', PasswordType::class,array('label' => 'Password','attr' => array('class' => 'form-control')))
       
        ->add('login', SubmitType::class,array('attr' => array('class' => 'btn btn-primary btn-md btn-block','style'=>'margin-top:10px'),'label' => 'Login'))

        ->getForm();

      $form->handleRequest($request);
        
      return $this ->render('AppBundle:bulutron:login.html.twig',array(
        'form' => $form->createView()
        ));
    }


    /**
     * @Route("/logout" , name = "logout")
     */
    public function logoutAction(){

    return $this-> render('AppBundle:bulutron:logout.html.twig');


    }


}











?>
