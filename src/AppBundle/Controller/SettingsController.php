<?php
namespace AppBundle\Controller;

use AppBundle\Entity\Enduser;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\RadioType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\JsonResponse;

class SettingsController extends Controller
{

  /**
   * @Route("/setting", name = "setting")
   *@Method({"GET", "POST"})
   */
  public function settingAction(Request $request){
    $user = $this->get('security.token_storage')->getToken()->getUser();
    $userId = $user->getId();

    $logger = $this->container->get('app.enduser_preferences');

    $sharing_enabled = $logger->getPreference()['sharing_enabled'];
    $sharing_notification_enabled = $logger->getPreference()['sharing_notification_enabled'];
    $api_enabled = $logger->getPreference()['api_enabled'];

    $form = $this->createFormBuilder()
    ->add('sharing_enabled',ChoiceType::class,array('label'=> "Başkalarının benimle dosya paylaşmasına",
       'choices' => array(
         'İzin Veriyorum' => '1',
         'İzin Vermiyorum' => '0'
       ),
       'multiple' => false,
       'expanded' => true,
       'data'=> $sharing_enabled

   ))
   ->add('sharing_notification_enabled',ChoiceType::class,array('label'=> "Paylaştığım dosyalar erişildiğinde e-mail ile bilgilendirilmek",
      'choices' => array(
        'İstiyorum' => '1',
        'İstemiyorum' => '0'
      ),
      'multiple' => false,
      'expanded' => true,
      'data' => $sharing_notification_enabled
  ))
  ->add('api_enabled',ChoiceType::class,array('label'=> "Dosyalarıma ait bilgilere API ile erişime",
     'choices' => array(
       'İzin Veriyorum' => '1',
       'İzin Vermiyorum' => '0'
     ),
     'multiple' => false,
     'expanded' => true,
     'data' =>$api_enabled
 ))
 ->add('add', SubmitType::class,array('attr' => array('class' => 'btn btn-success btn-md'),'label' => 'Kaydet'))
   ->getForm();
    $form->handleRequest($request);

    if($form->isSubmitted() && $form->isValid()){

      $sharing_enabled = $form['sharing_enabled']->getData();
      $sharing_notification_enabled = $form['sharing_notification_enabled']->getData();
      $api_enabled = $form['api_enabled']->getData();

      $em = $this-> getDoctrine()->getManager();
      $kayit = $em->getRepository('AppBundle:Enduser')->findOneBy(array('id' => $userId));

      if($kayit){
        $arr = ['sharing_enabled' => $sharing_enabled, 'sharing_notification_enabled'=>$sharing_notification_enabled, 'api_enabled'=>$api_enabled];
        $kayit->setPreferences($arr);
        $em ->persist($kayit);
        $em ->flush();
        $this -> AddFlash(
          'notice',
           'Değişiklikler Kaydedilmiştir.'
       );
      }



    }



    return $this->render('AppBundle:bulutron:settings.html.twig',array(
      'form' => $form->createView()
    ));



  }
}






?>
