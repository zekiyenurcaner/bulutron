<?php
namespace AppBundle\Services;

use AppBundle\Entity\Enduser;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\User\UserInterface;


class EnduserPreferences

{

private $em;
private $user;
private $storage;
private $sharing_enabled;
private $sharing_notification_enabled;
private $api_enabled;

public function __construct(AuthorizationCheckerInterface $user, EntityManager $em,TokenStorageInterface $storage,$sharing_enabled,$sharing_notification_enabled,$api_enabled)
{

  $this->user = $user;
  $this->em = $em;
  $this->storage = $storage;
  $this->sharing_enabled = $sharing_enabled;
  $this->sharing_notification_enabled = $sharing_notification_enabled;
  $this->api_enabled = $api_enabled;
}
public function getPreference()
{
  if ($this->user->isGranted('ROLE_ADMIN')) {
      return $this->storage->getToken()->getUser()->getPreferences();
        }
      }

}



?>
