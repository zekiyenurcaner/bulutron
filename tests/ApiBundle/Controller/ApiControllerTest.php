<?php

namespace ApiBundle\Tests\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ApiControllerTest extends WebTestCase
{
    public function testPostFilesAction()
    {   
        $this->setApiStatus("0");
        $client = $this->getApiFiles();
        $this->assertEquals(401,$client->getResponse()->getStatusCode());
    }

    public function testPostQuotaAction()
    {   
        $this->setApiStatus("0");
        $client = $this->getApiQuota();
        $this->assertEquals(401,$client->getResponse()->getStatusCode());
    }

    public function testUserFile()
    {
        $this->setApiStatus("1");
        $client = $this->getApiFiles();
        $data = $client->getResponse()->getContent();
        $this->assertEquals(200,$client->getResponse()->getStatusCode());
        $this->assertJson($data);
        $this->assertContains("filename", $data,'3',true);
    }

    public function testUserQuota()
    {
        $size = "245547";
        $this->setApiStatus("1");
        $client = $this->getApiQuota();
        $data = $client->getResponse()->getContent();
        $this->assertEquals(200,$client->getResponse()->getStatusCode());
        $this->assertJson($data);
        $this->assertEquals(3,json_decode($data,true)['file_count']);
        $this->assertEquals($size,json_decode($data,true)['disk_usage']);
    }

//İlgili kişiyi db de sorgulayıp ayarını çağırıyor
    protected function setApiStatus($boolen)
    {   
        $client = $this->getApiFiles();
        $kernel = $this->createKernel();
        $kernel->boot();
        $user = $kernel->getContainer()->get('doctrine')->getRepository('AppBundle:Enduser')->findOneBy(array('email'=>json_decode($client->getRequest()->getContent(), true)['email']));
        if($user)
        {
            $preferences=array('sharing_enabled'=>"1",'sharing_notification_enabled'=>"1",'api_enabled'=>$boolen);
            $user=$user->setPreferences($preferences);
            $em=$kernel->getContainer()->get('doctrine.orm.entity_manager');
            $em->persist($user);
            $em->flush();
        }

        /**
         * @var CloudFile $file
         */
        $file=$kernel->getContainer()->get('doctrine')->getRepository('AppBundle:CloudFile')->findBy(array('enduser'=>$user->getId()));
        return count($file);

    }

    //İlgili kişiyi api/files  post ediyor
    protected function getApiFiles()
    {
        $client = static::createClient(array(
            'environment' => 'test',
            'debug'       => true,
        ));
        $method ="POST";
        $url = "/api/files";
        $content = ["email" => 'mehmetdemir@mehmetdemir.com', "password" => "123456", "type"=>"apiTest"];
        $client->request($method, $url, [], [], [], json_encode($content));
        return $client;
    }
    
    //İlgili kişiyi api/quota  post ediyor
    protected function getApiQuota()
    {
        $client = static::createClient(array(
            'environment' => 'test',
            'debug'       => true,
        ));
        $method ="POST";
        $url = "/api/quota";
        $content = ["email" => 'mehmetdemir@mehmetdemir.com', "password" => "123456", "type"=>"apiTest"];
        $client->request($method, $url, [], [], [], json_encode($content));
        return $client;
    }

}
