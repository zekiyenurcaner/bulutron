<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UserLoginPageControllerTest extends WebTestCase
{   

    //Kullanıcı Adıyla Giriş yaptığında Adıyla karşılaşıyor mu?
    public function testNameLogIn()
    {
        $client = $this->login('mehmetdemir', '123456');
        $this->assertEquals(302, $client->getResponse()->getStatusCode());
        $client->followRedirects();
        $crawler = $client->request('GET', '/anasayfa'); 
        $this->assertContains('Mehmet Demir', $crawler->filter('h2')->text());
        
    }

    //Kullanıcı Email adresiyle giriş yaptığında Adıyla Karşılaşıyor mu?
    public function testEmailLogIn()
    {
        $client = $this->login('mehmetdemir@mehmetdemir.com', '123456');
        $this->assertEquals(302, $client->getResponse()->getStatusCode());
        $client->followRedirects();
        $crawler = $client->request('GET', '/anasayfa'); 
        $this->assertContains('Mehmet Demir', $crawler->filter('h2')->text());
    }

    //Kullanıcı Adı ve şifresi Yanlış Girilmişse Login sayfası tekrar döner
    public function testWrongName()
    {
        $client = $this->login('mehmet', '123');
        $this->assertEquals(302, $client->getResponse()->getStatusCode());
        $client->followRedirects();
        $crawler = $client->request('GET', '/anasayfa'); 
        $client->getResponse()->getContent();
    }

    //Hatalı email doğru şifre Login sayfası tekrar döner 
    public function testWrongEmail()
    {
        $client = $this->login('m@gmail.com','123456');
        $this->assertEquals(302,$client->getResponse()->getStatusCode());
        $client->followRedirects();
        $crawler = $client->request('GET','/anasayfa');
        $client->getResponse()->getContent();

    }

    protected function login($user,$pass)
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/login');

        /**
         * Form
         */
        $form = $crawler->filter('form')->form();
        $form['form[_username]'] = $user;
        $form['form[_password]'] = $pass;
        $crawler = $client->submit($form);
        return $client;

    }

}
?>