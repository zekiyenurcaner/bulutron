<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UserRegisterControllerTest extends WebTestCase
{   
    
    public function testRegisterEmptyName()
    {
        $client = $this->register("","nur@gmail.com","nur","123456","123456","2","Ankara");
        $this->assertContains('İsim Boş Geçilemez', $client->getResponse()->getContent());
    }

    public function testRegisterEmptyEmail()
    {
        $client = $this->register("nur","","nur","123456","123456","2","Ankara");
        $this->assertContains('Email Boş Geçilemez', $client->getResponse()->getContent());
    }

    public function testCity()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/newuser');
        $value = $crawler->filter('#form_city');
        $this->assertNotEmpty($value);
    }

    public function testRegisterEmptymaidenName()
    {
        $client = $this->register("nur","nur@gmail.com","nur","123456","123456","1","Ankara");
        $this->assertContains('Hatalı İşlem', $client->getResponse()->getContent());
    }

    public function testRegisterUsedUsername()
    {
        $client = $this->register("nur","nur@gmail.com","mehmetdemir","123456","123456","2","Ankara");
        $this->assertContains('Kullanılan Username', $client->getResponse()->getContent());
    }

    public function testRegisterUsedEmail()
    {
        $client = $this->register("nur","mehmetdemir@mehmetdemir.com","nur","123456","123456","2","Ankara");
        $this->assertContains('Önceden Kayıtlı mail adresi',$client->getResponse()->getContent());
    }

    protected function register($name,$email,$username,$passfirst,$passsecond,$sex,$city)
        {
            $client = static::createClient();
            $crawler = $client->request('GET', '/newuser');
    
            /**
             * Form
             */
            $form = $crawler->filter('form')->form();
            $form['form[name]'] = $name;
            $form['form[email]'] = $email;
            $form['form[username]'] = $username;
            $form['form[password][first]'] = $passfirst;
            $form['form[password][second]'] = $passsecond;
            $form['form[sex]'] = $sex;
            $form['form[city]'] = $city;
            $crawler = $client->submit($form);
            return $client;
    
        }
        
    

}
?>