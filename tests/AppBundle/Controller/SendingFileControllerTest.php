<?php

 use Tests\AppBundle\Controller\CommandTestCase;
 use Doctrine\ORM\EntityManager;
 use AppBundle\Entity\SendEmail;
 
 class SendingFileControllerFile extends CommandTestCase
 {  
  
    public function testSendingFile()
    {   
        $client = self::createClient();
        $this->sendingFile('test','zekiyenur.caner@yt.com.tr','zekiyenur.caner@yt.com.tr',0,'/home/zekiyenur/Desktop/files/3e780e43bbfc226e147e7ec5ea9b81f0.pdf');
        $output = $this->runCommand($client,'send:email');     
        $this->assertContains('Sending Mail', $output);     
    }
    protected function sendingFile($content,$gonderen,$alici,$durum,$file)
    {
        $kernel = $this->createKernel();
        $kernel->boot();
        $sending = $kernel->getContainer()->get('doctrine')->getRepository('AppBundle:SendEmail')->findBy([]);
       foreach($sending as $sendingfile){
        $sendingfile->setContent($content);
        $sendingfile->setGonderen($gonderen);
        $sendingfile->setAlici($alici);
        $sendingfile->setDurum($durum);
        $sendingfile->setFile($file);
        $em=$kernel->getContainer()->get('doctrine.orm.entity_manager');
        $em->persist($sendingfile);
        $em->flush();
       }
        return $sending;

    }


 }



















?>