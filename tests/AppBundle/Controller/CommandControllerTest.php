<?php

 use Tests\AppBundle\Controller\CommandTestCase;
 use Doctrine\ORM\EntityManager;
 use AppBundle\Entity\BatchDownloadRequest;
 
 class CommandControllerTest extends CommandTestCase
 {
     public function testBatchDownloadCommand()
     {  
	    $client = self::createClient();
        $this->batchDownloadAction(14,'bulutron-4.pdf',0,'9bfcf13f96a78c65ad2aaa954a3bc21c.pdf',2);
        $this->batchDownloadAction(17,'bulutron-6.pdf',0,'d6fe37416acc64399dac5415358faa68.pdf',2);
        $output = $this->runCommand($client, "app:batchdownload");
        $kernel = $this->createKernel();
        $kernel->boot();
        $files = $kernel->getContainer()->get('doctrine')->getRepository('AppBundle:BatchDownloadRequest')->findOneBy(array('personId'=>2));
        $st = $files->getStatus();
        $this->assertEquals(1,$st);
        $this->assertContains('Sending Mail', $output);
     }

     protected function batchDownloadAction($dosyaId,$dosyaAdi,$status,$dosyaPath,$personId)
     {
        $kernel = $this->createKernel();
        $kernel->boot();
        $sending = $kernel->getContainer()->get('doctrine')->getRepository('AppBundle:BatchDownloadRequest')->findBy([]);
       foreach($sending as $sendingfile){
        $sendingfile->setDosyaId($dosyaId);
        $sendingfile->setDosyaAdi($dosyaAdi);
        $sendingfile->setStatus($status);
        $sendingfile->setDosyaPath($dosyaPath);
        $sendingfile->setPersonId($personId);
        $em=$kernel->getContainer()->get('doctrine.orm.entity_manager');
        $em->persist($sendingfile);
        $em->flush();
       }
        return $sending;
     }
 }
 ?>