<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class HomePageControllerTest extends WebTestCase
{   
    //Anonim kullanıcı anasayfadan giriş sayfasına yönlendirilir
    public function testAnonUser()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/anasayfa');
        $this->assertEquals(302, $client->getResponse()->getStatusCode());
        $crawler = $client->followRedirect();        
        $crawler = $client->request('GET', '/login');
        $this->assertContains('Bulutron Login', $crawler->filter('h2')->text());

        
    }

}
?>