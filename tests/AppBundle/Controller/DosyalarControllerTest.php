<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DosyalarControllerTest extends WebTestCase
{   

    //Kullanıcı Login Test
    public function testLogIn()
    {

        $client = $this->login('mehmetdemir', '123456');
        $this->assertEquals(302, $client->getResponse()->getStatusCode());
        $client->followRedirects();
        $crawler = $client->request('GET', '/anasayfa'); 
        $client->getResponse()->getContent();
        
    }
    protected function login($user,$pass)
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/login');

        /**
         * Form
         */
        $form = $crawler->filter('form')->form();
        $form['form[_username]'] = $user;
        $form['form[_password]'] = $pass;
        $crawler = $client->submit($form);
        return $client;

    }

    // Anon. Kullanıcı dosyaların sayfasında 500 hatası alır
    public function testError()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/index');
        $this->assertEquals(500, $client->getResponse()->getStatusCode());
        $this->assertContains('500 Interval Server Error', $crawler->filter('h1')->text());
    }

    //Login User Dosyalar sayfasında 200 alır
    public function testUserLogin()
    {
        $client = $this->login('mehmetdemir', '123456');
        $this->assertEquals(302, $client->getResponse()->getStatusCode());
        $client->followRedirects();
        $crawler = $client->request('GET', '/index'); 
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testFileList()
    {
        $client = $this->login('mehmetdemir','123456');
        $client->followRedirects();
        $crawler = $client->request('GET', '/index');
        $this->assertEquals(3,$crawler->filter('td.say')->count());
        $this->assertContains('3 dosyanız',$client->getResponse()->getContent());
        $this->assertContains('387.33 MB alan',$client->getResponse()->getContent());
        $link = $crawler->selectLink('Sil')->link();
        $client->click($link);
        $this->assertNotEquals('10th November 2017	237.7MB',$client->getResponse()->getContent());
         
    }
   
}
?>